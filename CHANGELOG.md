## Changelog

This tiny linux bash framework is a rolling release program. That's why you won't see version numbers but serialized date in the form 'YYYmmdd.HHMM' instead.

## [1.1.1] - 2020-11-16

### Added

-	nothing. This is just a test

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [1.0.0] - 2020-11-16

### Added

### Changed

-	moved project to gitlab.com
