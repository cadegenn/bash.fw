```
DESCRIPTION: ${BASENAME} do some things
    USAGE: ${BASENAME} [-d] [-dev] [-h] [-y]
         -h    help screen (this screen)
         -d    debug mode: print VARIABLE=value pairs
         -dev  devel mode: print additional development data
         -ask  ask for user confirmation for each individual eexec() call
               NOTE: it is not affected by -y
         -y    assume 'yes' to all questions
         -s    simulate: do not really execute commands
```
